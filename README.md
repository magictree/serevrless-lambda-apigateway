<!--
title: 'AWS Serverless Edu-services examples'
description: 'This template demonstrates how to deploy a NodeJS function running on AWS Lambda using the traditional Serverless Framework.'
layout: Doc
framework: v3
platform: AWS
language: nodeJS
priority: 1
authorLink: 'https://gitlab.com/magictree/serevrless-lambda-apigateway'
authorName: 'agustriadji'
-->

# AWS Serverless Lambda API Gateway edu-service

This template demonstrates how to deploy a NodeJS function running on AWS Lambda using the traditional Serverless Framework.
please refer to our [documentation](https://documenter.getpostman.com/view/559121/2s9YC7UXbK) postman.

## Endpoint functions

```
endpoints:
  GET - https://zucipwh2rh.execute-api.us-east-1.amazonaws.com/dev/chart-read/{chartId}
  GET - https://zucipwh2rh.execute-api.us-east-1.amazonaws.com/dev/chart-get
  POST - https://zucipwh2rh.execute-api.us-east-1.amazonaws.com/dev/chart-add
  PUT - https://zucipwh2rh.execute-api.us-east-1.amazonaws.com/dev/chart-edit/{courseId}
  DELETE - https://zucipwh2rh.execute-api.us-east-1.amazonaws.com/dev/chart-remove/{courseId}
  POST - https://zucipwh2rh.execute-api.us-east-1.amazonaws.com/dev/chart-checkout
  GET - https://zucipwh2rh.execute-api.us-east-1.amazonaws.com/dev/transactions-get
  POST - https://zucipwh2rh.execute-api.us-east-1.amazonaws.com/dev/transactions-create
  PUT - https://zucipwh2rh.execute-api.us-east-1.amazonaws.com/dev/transactions-confirm/{invoiceNumber}
  PUT - https://zucipwh2rh.execute-api.us-east-1.amazonaws.com/dev/transactions-cancel/{invoiceNumber}
  GET - https://zucipwh2rh.execute-api.us-east-1.amazonaws.com/dev/course-get
  GET - https://zucipwh2rh.execute-api.us-east-1.amazonaws.com/dev/course-read/{courseId}
  POST - https://zucipwh2rh.execute-api.us-east-1.amazonaws.com/dev/course-add
  PUT - https://zucipwh2rh.execute-api.us-east-1.amazonaws.com/dev/course-edit/{courseId}
  PUT - https://zucipwh2rh.execute-api.us-east-1.amazonaws.com/dev/course-update-status/{courseId}
```

## Usage Offline

run serverless-offline, host default http://localhost:3000, EX: http://localhost:3000/course-get

```
$ sls offline start
```

### Deployment

In order to deploy the example, you need to run the following command:

```
$ serverless deploy
```

After running deploy, you should see output similar to:

```bash
Deploying aws-node-project to stage dev (us-east-1)

✔ Service deployed to stack aws-node-project-dev (112s)

functions:
  hello: aws-node-project-dev-hello (1.5 kB)
```
