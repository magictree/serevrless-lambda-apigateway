"use strict";

const {
  addItemChart_Service,
  editItemChart_Service,
  getChart_Service,
  removeItemChart_Service,
  checkoutChart_Service,
} = require("./chart.service");
const APIResponse = require("../common/API_RESPONSE");

/**
 * @function getChart_Main lambda function for add get chart
 * @method GET
 * @param {*} event
 * @returns {Promise<*>} API Response
 */
exports.getChart_Main = async (event) => {
  let pathParameters = null;
  if (event.pathParameters) pathParameters = event.pathParameters;

  const chartId = pathParameters ? {...pathParameters.chartId} : false;
  const res = await getChart_Service(chartId);
  return await APIResponse(res);
};

/**
 * @function addItemChart_Main lambda function for add item to chart
 * @method POST
 * @param {*} event
 * @returns {Promise<*>} API Response
 */
exports.addItemChart_Main = async (event) => {
  try {
    const listItem = JSON.parse(event.body);
    const res = await addItemChart_Service(listItem);
    return await APIResponse(res);
  } catch (error) {
    return await APIResponse();
  }
};

/**
 * @function editItemChart_Main lambda function for edit quantity item chart
 * @param {*} event
 * @method PUT
 * @returns {Promise<*>} API Response
 */
exports.editItemChart_Main = async (event) => {
  let pathParameters = null;
  if (event.pathParameters) pathParameters = event.pathParameters;

  const body = JSON.parse(event.body);
  const res = await editItemChart_Service({...body, ...pathParameters});
  return await APIResponse(res);
};

/**
 * @function removeItemChart_Main, lambda function for remove item chart
 * @method DELETE
 * @param {*} event
 * @returns {Promise<*>} API Response
 */
exports.removeItemChart_Main = async (event) => {
  let pathParameters = null;
  if (event.pathParameters) pathParameters = event.pathParameters;

  const valid = await removeItemChart_Service({...pathParameters});
  return await APIResponse(valid);
};

/**
 * @function checkoutChart_Main lambda function for checkoutChart
 * @method POST
 * @param {*} event
 * @returns {Promise<*>} API Response
 */
exports.checkoutChart_Main = async (event) => {
  const listItem = JSON.parse(event.body);
  const res = await checkoutChart_Service(listItem);
  return await APIResponse(res);
};
