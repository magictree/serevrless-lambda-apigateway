const Joi = require('joi');

const item = {
  courseId: Joi.string().invalid('').required(),
  quantity: Joi.number().positive().required(),
  price: Joi.number().positive().required(),
};

const schemaItem = Joi.object().keys({
  listItem: Joi.array().items(Joi.object().keys(item)),
});

const schemaEditItem = Joi.object().keys(item);

const schemaRemoveItem = Joi.object().keys({courseId: item.courseId});

/**
 * addItem_Spec
 * add or checkout item to chart
 * @param {object} payload
 * @param {Array.object<{courseId: string, quantity: number, price:number}>} payload.listItem Only allow courseId and quantity
 * @example * var payload = { listItem: [{courseId:'abcdefg', quantity:2, price:200}]}; *
 * @returns {Promise<{message: string, error: boolean, data: object}>} data = listItem data
 */
exports.addItem_Spec = async (payload) => {
  const {value, error} = schemaItem.validate(payload);
  return {message: error ? error : 'ok', error: error ? true : false, data: value};
};

/**
 * editItem_Spec
 * edit item chart
 * @param {object} payload Only allow courseId and quantity
 * @param {string} payload.courseId
 * @param {number} payload.quantity
 * @param {number} payload.price
 * @example * var payload = {courseId:'abcdefg', quantity:2, price:number}; *
 * @returns {Promise<{message: string, error: boolean, data: object}>}
 */
exports.editItem_Spec = async (payload) => {
  const {value, error} = schemaEditItem.validate(payload);
  return {message: error ? error : 'ok', error: error ? true : false, data: value};
};

/**
 * removeItem_Spec
 * remove item chart
 * @param {object} payload Only allow courseId
 * @param {string} payload.courseId
 * @example * var payload = {courseId:'abcdefg'}; *
 * @returns {Promise<{message: string, error: boolean, data: object}>}
 */
exports.removeItem_Spec = async (payload) => {
  const {value, error} = schemaRemoveItem.validate(payload);
  return {message: error ? error : 'ok', error: error ? true : false, data: value};
};
