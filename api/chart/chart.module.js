const mongoose = require("mongoose");
const DB = require("../../database");
const chart = require("../../database/schema/chart");

const connectionFactory = async function (dbName = "ecommerce", schemaName = chart.model) {
  const conn = await DB(dbName);
  const model = await conn.model(schemaName, chart.schema);
  return {conn, model};
};

/**
 * getChart_Module
 * @function getChart_Module module for get chart data
 * @param {object} payload
 * @param {string} paylaod.chartId Object Id chart (optional)
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
exports.getChart_Module = async function (payload) {
  const {conn, model} = await connectionFactory();
  let query = model.find();
  if (payload) query = model.findById(payload.chartId);
  const data = await query.exec();

  let response = {error: false, message: "ok", data};

  await conn.close();
  return {...response};
};

/**
 * addItemChart_Module
 * @function addItemChart_Module module for add item to chart
 * @param {object} payload
 * @param {Array.object<{courseId:string, quantity: number, price:number}>} payload.listItem  payload from request
 * @example *var payload = { listItem :[{ courseId:'123', quantity:1, price:1000 },{ courseId:'345', quantity:2,price:1000 },...n]}*
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
exports.addItemChart_Module = async function (payload) {
  const {conn, model} = await connectionFactory();
  let action = await model.findOne().lean();
  if (!action) {
    action = new model(payload);
    action = await action.save();
  } else {
    action.listItem = [...action.listItem, ...payload.listItem];
    await model.findOneAndUpdate({"_id": action._id}, {"listItem": action.listItem});
  }

  let response = {error: false, message: "ok", data: {}};
  if (!action) {
    response.error = true;
    response.message = "store failed";
  }

  await conn.close();
  return {...response};
};

/**
 * editItemChart_Module
 * @function editItemChart_Module module for edit quantity item chart by courseId
 * @param {object<{courseId:string, quantity: number}>} payload  payload from request
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
exports.editItemChart_Module = async function (payload) {
  let {conn, model} = await connectionFactory();
  let action = await model
    .findOneAndUpdate(
      {"listItem.courseId": payload.courseId},
      {"listItem.$.quantity": payload.quantity}
    )
    .lean();
  let response = {error: false, message: "ok", data: action};
  if (!action) {
    response.error = true;
    response.message = "edit failed";
  }

  await conn.close();
  return {...response};
};

/**
 * removeItemChart_Module
 * @function editItemChart_Module module for remove chart item by courseId
 * @param {object<{courseId:string}>} payload  payload from request
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
exports.removeItemChart_Module = async function (payload) {
  const {conn, model} = await connectionFactory();
  let action = await model
    .findOneAndUpdate(
      {"listItem.courseId": payload.courseId},
      {
        "$pull": {
          "listItem": {
            "courseId": payload.courseId,
          },
        },
      }
    )
    .lean();

  let response = {error: false, message: "ok", data: {}};
  if (!action) {
    response.error = true;
    response.message = "remove failed";
  }

  await conn.close();
  return {...response};
};

/**
 * checkoutChart_Module
 * @function checkoutChart_Module module for remove item saat checkout
 * @param {Array<string>} payload  array from courseId value
 * @example *var payload=['asdasd','dasdasd']*
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
exports.checkoutChart_Module = async function (payload) {
  try {
    const {conn, model} = await connectionFactory();
    const action = await model
      .findOneAndUpdate(
        {
          "listItem.courseId": {$in: payload},
        },
        {
          $pull: {
            "listItem": {
              "courseId": {$in: payload},
            },
          },
        }
      )
      .lean();

    let response = {error: false, message: "ok", data: {}};
    if (!action) {
      response.error = true;
      response.message = "checkout failed";
    }

    await conn.close();
    return {...response};
  } catch (error) {
    console.log(error, 999);
    return {error: true, message: error, data: null};
  }
};
