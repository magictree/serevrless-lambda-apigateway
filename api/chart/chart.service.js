"use strict";

const {v4: uuidv4} = require("uuid");
// const {hitApi} = require("../common/lib");
const {checkoutTransactions_Service} = require("../transactions/transactions.service");
const {addItem_Spec, editItem_Spec, removeItem_Spec} = require("./chart.spec");
const {
  getChart_Module,
  addItemChart_Module,
  editItemChart_Module,
  removeItemChart_Module,
  checkoutChart_Module,
} = require("./chart.module");

/**
 * getChart_Service
 * @function addItemChart_Service services get chart
 * @param {object} payload
 * @param {string} paylaod.chartId Object Id chart (optional)
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
async function getChart_Service(payload) {
  try {
    const res = await getChart_Module(payload);
    return {...res};
  } catch (error) {
    return {error: true, message: error, data: {}};
  }
}

/**
 * addItemChart_Service
 * @function addItemChart_Service services add item to chart
 * @param {object} payload
 * @param {Array.object<{courseId:string, quantity: number, price: number}>} payload.listItem  payload from request
 * @example *var payload = { listItem :[{ courseId:'123', quantity:1, price:1000 },{ courseId:'345', quantity:2, price:4000 },...n]}*
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
async function addItemChart_Service(payload) {
  try {
    let res = await addItem_Spec(payload);
    if (!res.error) {
      res = await addItemChart_Module(payload);
    }
    return {...res};
  } catch (error) {
    return {error: true, message: error, data: {}};
  }
}

/**
 * buildDataTransactions
 * @private
 * @function buildDataTransactions
 * @param {object} payload
 * @param {Array.object<{courseId:string, quantity: number, price:number}>} payload.listItem  payload from request
 * @example *var payload = { listItem :[{ courseId:'123', quantity:1, price:1000 },{ courseId:'345', quantity:2, price:2000 },...n]}*
 * @returns {Promise<{dataTransactions: object, arrCourseId:array }>}
 */
async function buildDataTransactions(payload) {
  try {
    let arrCourseId = [];
    let grandTotal = 0;
    const invoiceNumber = uuidv4();

    payload.map((item) => {
      item.totalPrice = item.price * item.quantity;
      grandTotal = grandTotal + item.totalPrice;
      arrCourseId.push(item.courseId);
    });

    const dataTransactions = {
      invoiceNumber,
      price: grandTotal,
      listItem: payload,
      status: "pending",
    };
    return {dataTransactions, arrCourseId};
  } catch (error) {
    console.log(error);
    return false;
  }
}

/**
 * hit transaction
 */

/**
 * checkoutChart_Service
 * @function checkoutChart_Service services add item to chart
 * @param {object} payload
 * @param {Array.object<{courseId:string, quantity: number, price:number}>} payload.listItem  payload from request
 * @example *var payload = { listItem :[{ courseId:'123', quantity:1, price:1000 },{ courseId:'345', quantity:2, price:2000 },...n]}*
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
async function checkoutChart_Service(payload) {
  try {
    let res = await addItem_Spec(payload);

    if (!res.error) {
      const {dataTransactions, arrCourseId} = await buildDataTransactions(res.data.listItem);
      if (!dataTransactions) {
        res.message = "checkout failed";
        res.error = true;
        res.data = {};
      } else {
        res = await checkoutChart_Module(arrCourseId);

        if (!res.error) {
          res = await checkoutTransactions_Service(dataTransactions);
          // res = await hitApi({
          //   url: "http://localhost:3000/transactions-create",
          //   method: "POST",
          //   body: dataTransactions,
          //   headers: {
          //     "Content-Type": "application/json",
          //   },
          // });
        }
      }
    }
    return {...res};
  } catch (error) {
    console.log(error, "ERROR");
    return {error: true, message: error, data: {}};
  }
}

/**
 * editItemChart_Service
 * @function editItemChart_Service services edit quantity item
 * @param {object} payload
 * @param {string} payload.courseId
 * @param {number} payload.quantity
 * @param {number} payload.price
 * @example *var payload = { courseId:'123', quantity:1, price:100 }*
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
async function editItemChart_Service(payload) {
  try {
    let res = await editItem_Spec(payload);
    if (!res.error) {
      res = await editItemChart_Module(payload);
    }
    return {...res};
  } catch (error) {
    return {error: true, message: error, data: {}};
  }
}

/**
 * removeItemChart_Service
 * @function removeItemChart_Service services remove chart item
 * @param {object} payload
 * @param {string} payload.courseId
 * @example *var payload = { courseId:'123' }*
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
async function removeItemChart_Service(payload) {
  try {
    let res = await removeItem_Spec(payload);
    if (!res.error) {
      res = await removeItemChart_Module(payload);
    }
    return {...res};
  } catch (error) {
    return {error: true, message: error, data: {}};
  }
}

module.exports = {
  getChart_Service,
  addItemChart_Service,
  editItemChart_Service,
  removeItemChart_Service,
  checkoutChart_Service,
};
