"use strict";

const {v4: uuidv4} = require("uuid");
const {
  addCourse_Spec,
  detailCourse_Spec,
  editCourse_Spec,
  updateStatusCourse_Spec,
} = require("./course.spec");
const {
  getCourse_Module,
  addCourse_Module,
  editCourse_Module,
  updateStatusCourse_Module,
} = require("./course.module");

/**
 * getCourse_Service
 * @function getCourse_Service services get chart
 * @param {string} courseId optional
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
async function getCourse_Service(courseId) {
  try {
    let res = null;
    if (courseId) {
      res = await detailCourse_Spec({courseId});
    }
    res = await getCourse_Module(courseId || false);
    return {...res};
  } catch (error) {
    return {error: true, message: error, data: {}};
  }
}

/**
 * addCourse_Service
 * @function addCourse_Service services add item to chart
 * @param {object} payload
 * @param {string} payload.title
 * @param {string} payload.description allow null
 * @param {number} payload.price default 0
 * @param {boolean} payload.is_free default true
 * @param {string} payload.author optional
 * @param {string} payload.status optional default active
 * @example * var payload = { title: "grammer lesson", description: "this description course", price:2000, is_free:false, auhtor:"yourname" }; *
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
async function addCourse_Service(payload) {
  try {
    let res = await addCourse_Spec(payload);
    if (!res.error) {
      res = await addCourse_Module(payload);
    }
    return {...res};
  } catch (error) {
    return {error: true, message: error, data: {}};
  }
}

/**
 * editCourse_Service
 * @function editCourse_Service services edit course
 * @param {object} payload
 * @param {string} payload.courseId is required
 * @param {string} payload.title
 * @param {string} payload.description allow null
 * @param {number} payload.price default 0
 * @param {boolean} payload.is_free default true
 * @param {string} payload.author optional
 * @param {string} payload.status optional default active
 * @example * var payload = { courseId:'213123', title: "grammer lesson", description: "this description course", price:2000, is_free:false, auhtor:"yourname" }; *
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
async function editCourse_Service(payload) {
  try {
    let res = await editCourse_Spec(payload);
    if (!res.error) {
      res = await editCourse_Module(payload);
    }
    return {...res};
  } catch (error) {
    return {error: true, message: error, data: {}};
  }
}

/**
 * upadteStatusCourse_Service
 * @function upadteStatusCourse_Service services udpate status course
 * @param {object} payload
 * @param {string} payload.courseId is required
 * @param {string} payload.status optional default active
 * @example * var payload = { courseId:'213123', auhtor:"yourname", status:"inactive" }; *
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
async function upadteStatusCourse_Service(payload) {
  try {
    let res = await updateStatusCourse_Spec(payload);
    if (!res.error) {
      res = await updateStatusCourse_Module(payload);
    }
    return {...res};
  } catch (error) {
    return {error: true, message: error, data: {}};
  }
}

module.exports = {
  getCourse_Service,
  addCourse_Service,
  editCourse_Service,
  upadteStatusCourse_Service,
};
