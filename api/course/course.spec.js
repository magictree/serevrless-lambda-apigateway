const Joi = require('joi');

const item = {
  courseId: Joi.string().allow(null),
  title: Joi.string().invalid('').required(),
  description: Joi.string().allow(null),
  price: Joi.number().default(0).min(0).required(),
  is_free: Joi.boolean().default(true).required(),
  author: Joi.string(),
  status: Joi.string().allow('active', 'inactive').default('active'),
};
const schemaItem = Joi.object().keys(item);

const schemaRemoveItem = Joi.object().keys({courseId: item.courseId, status: item.status});

const schemaGetItem = Joi.object().keys({courseId: item.courseId});

/**
 * addCourse_Spec
 * add course
 * @param {object} payload
 * @param {string} payload.title
 * @param {string} payload.description allow null
 * @param {number} payload.price default 0
 * @param {boolean} payload.is_free default true
 * @param {string} payload.author optional
 * @param {string} payload.status optional default active
 * @example * var payload = { title: "grammer lesson", description: "this description course", price:2000, is_free:false, auhtor:"yourname" }; *
 * @returns {Promise<{message: string, error: boolean, data: object}>} data = listItem data
 */
exports.addCourse_Spec = async (payload) => {
  const {value, error} = schemaItem.validate(payload);
  return {message: error ? error : 'ok', error: error ? true : false, data: value};
};

/**
 * editCourse_Spec
 * edit course by id
 * @param {object} payload
 * @param {string} payload.courseId is required
 * @param {string} payload.title is required
 * @param {string} payload.description allow null
 * @param {number} payload.price default 0
 * @param {boolean} payload.is_free default true
 * @param {string} payload.author optional
 * @param {string} payload.status optional default active
 * @example * var payload = { courseId: "1234", title: "grammer lesson", description: "this description course", price:2000, is_free:false, auhtor:"yourname" }; *
 * @returns {Promise<{message: string, error: boolean, data: object}>} data = listItem data
 */
exports.editCourse_Spec = async (payload) => {
  const {value, error} = schemaItem.validate(payload);
  return {message: error ? error : 'ok', error: error ? true : false, data: value};
};

/**
 * updateStatusCourse_Spec
 * update Status course by id
 * @param {object} payload
 * @param {string} payload.courseId is required
 * @param {string} payload.status is required
 * @example * var payload = { courseId: "1234", status: 'active or inactive' }; *
 * @returns {Promise<{message: string, error: boolean, data: object}>} data = listItem data
 */
exports.updateStatusCourse_Spec = async (payload) => {
  const {value, error} = schemaRemoveItem.validate(payload);
  return {message: error ? error : 'ok', error: error ? true : false, data: value};
};

/**
 * detailCourse_Spec
 * get all course or by id course
 * @param {string} courseId optional
 * @returns {Promise<{message: string, error: boolean, data: object}>} data = listItem data
 */
exports.detailCourse_Spec = async (payload) => {
  const {value, error} = schemaGetItem.validate(payload);
  return {message: error ? error : 'ok', error: error ? true : false, data: value};
};
