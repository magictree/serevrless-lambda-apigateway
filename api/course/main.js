"use strict";

const {
  getCourse_Service,
  addCourse_Service,
  editCourse_Service,
  upadteStatusCourse_Service,
} = require("./course.service");
const APIResponse = require("../common/API_RESPONSE");

/**
 * @function getCourse_Service lambda function for get course list or by id
 * @method GET
 * @param {*} event
 * @returns {Promise<*>} API Response
 */
exports.getCourse_Main = async (event) => {
  let pathParameters = null;
  if (event.pathParameters) pathParameters = event.pathParameters;

  const courseId = pathParameters ? pathParameters.courseId : false;
  const res = await getCourse_Service(courseId);
  return await APIResponse(res);
};

/**
 * @function addCourse_Main lambda function for add course
 * @method POST
 * @param {*} event
 * @returns {Promise<*>} API Response
 */
exports.addCourse_Main = async (event) => {
  const data = JSON.parse(event.body);
  const res = await addCourse_Service(data);
  return await APIResponse(res);
};

/**
 * @function editCourse_Main lambda function for edit chourse by id
 * @param {*} event
 * @method PUT
 * @returns {Promise<*>} API Response
 */
exports.editCourse_Main = async (event) => {
  let pathParameters = null;
  if (event.pathParameters) pathParameters = event.pathParameters;

  let body = JSON.parse(event.body);
  const res = await editCourse_Service({...body, ...pathParameters});
  return await APIResponse(res);
};

/**
 * @function upateStatusCourse_Main lambda function for update status course
 * @param {*} event
 * @method PUT
 * @returns {Promise<*>} API Response
 */
exports.upateStatusCourse_Main = async (event) => {
  let pathParameters = null;
  if (event.pathParameters) pathParameters = event.pathParameters;

  let body = JSON.parse(event.body);

  const res = await upadteStatusCourse_Service({...body, ...pathParameters});
  return await APIResponse(res);
};
