const DB = require('../../database');
const course = require('../../database/schema/course');

const connectionFactory = async function (dbName = 'lms', schemaName = course.model) {
  const conn = await DB(dbName);
  const model = conn.model(schemaName, course.schema);
  return {conn, model};
};

/**
 * getCourse_Module
 * @function getCourse_Module module for get course
 * @param {string} courseId optional
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
exports.getCourse_Module = async function (courseId) {
  const {conn, model} = await connectionFactory();
  let data = null;
  if (courseId) {
    data = await model.find({_id: courseId});
  } else {
    data = await model.find();
  }

  let response = {error: false, message: 'ok', data};

  await conn.close();
  return {...response};
};

/**
 * addCourse_Module
 * @function addCourse_Module module for add course
 * @param {object} payload
 * @param {string} payload.title is required
 * @param {string} payload.description allow null
 * @param {number} payload.price default 0
 * @param {boolean} payload.is_free default true
 * @param {string} payload.author optional
 * @param {string} payload.status optional default active
 * @example * var payload = { title: "grammer lesson", description: "this description course", price:2000, is_free:false, auhtor:"yourname" }; *
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
exports.addCourse_Module = async function (payload) {
  const {conn, model} = await connectionFactory();
  const action = new model(payload);
  const save = await action.save();

  let response = {error: false, message: 'ok', data: {}};

  if (!save) {
    response.error = true;
    response.message = 'store failed';
  }

  await conn.close();
  return {...response};
};

/**
 * editCourse_Module
 * @function editCourse_Module module for edit course
 * @param {object} payload
 * @param {string} payload.courseId is required
 * @param {string} payload.title is required
 * @param {string} payload.description allow null
 * @param {number} payload.price default 0
 * @param {boolean} payload.is_free default true
 * @param {string} payload.author optional
 * @param {string} payload.status optional default active
 * @example * var payload = { courseId: "1234", title: "grammer lesson", description: "this description course", price:2000, is_free:false, auhtor:"yourname" }; *
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
exports.editCourse_Module = async function (payload) {
  const {conn, model} = await connectionFactory();

  const _id = payload.courseId;
  delete payload.courseId;

  const action = await model.findOneAndUpdate(
    {
      _id,
    },
    {
      $set: {
        ...payload,
      },
    }
  );

  let response = {error: false, message: 'ok', data: {}};
  if (!action) {
    response.error = true;
    response.message = 'edit failed';
  }

  await conn.close();
  return {...response};
};

/**
 * updateStatusCourse_Module
 * @function updateStatusCourse_Module module for update status course
 * @param {object} payload
 * @param {string} payload.courseId is required
 * @param {string} payload.status optional default active
 * @example * var payload = { courseId: "1234", status:'inactive' }; *
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
exports.updateStatusCourse_Module = async function (payload) {
  const {conn, model} = await connectionFactory();

  const _id = payload.courseId;
  delete payload.courseId;

  const action = await model.findOneAndUpdate(
    {
      _id,
    },
    {
      $set: {
        ...payload,
      },
    }
  );

  let response = {error: false, message: 'ok', data: {}};
  if (!action) {
    response.error = true;
    response.message = 'update status failed';
  }

  await conn.close();
  return {...response};
};
