"use strict";
/**
 * only store transaction with status pending , after checkout
 */
const {
  cancelTransactions_Service,
  checkoutTransactions_Service,
  confirmTransactions_Service,
  getAllTransactions_Service,
} = require("./transactions.service");
const APIResponse = require("../common/API_RESPONSE");

/**
 * @function getTransactions_Main lambda function for get transactions
 * @method GET
 * @param {*} event
 * @returns {Promise<*>} API Response
 */
exports.getTransactions_Main = async function (event) {
  const res = await getAllTransactions_Service();
  return await APIResponse(res);
};

/**
 * @function createTransactions_Main lambda function for created transactions
 * @method POST
 * @param {*} event
 * @returns {Promise<*>} API Response
 */
exports.createTransactions_Main = async function (event) {
  const body = JSON.parse(event.body);
  const res = await checkoutTransactions_Service(body);
  return await APIResponse(res);
};

/**
 * @function confirmTransactions_Main lambda function for confirm transactions, "Harusnya pakai stripe weebhook"
 * @method PUT
 * @param {*} event
 * @returns {Promise<*>} API Response
 */
exports.confirmTransactions_Main = async function (event) {
  let pathParameters = {};
  let body = JSON.parse(event.body);
  if (event.pathParameters) pathParameters = event.pathParameters;

  const res = await confirmTransactions_Service({...pathParameters, ...body});
  return await APIResponse(res);
};

/**
 * @function cancelTransactions_Main lambda function for cancel transactions, "Harusnya pakai stripe weebhook or scheduler jika expired time"
 * @method PUT
 * @param {*} event
 * @returns {Promise<*>} API Response
 */
exports.cancelTransactions_Main = async function (event) {
  let pathParameters = {};
  if (event.pathParameters) pathParameters = event.pathParameters;

  const res = await cancelTransactions_Service({...pathParameters});
  return await APIResponse(res);
};
