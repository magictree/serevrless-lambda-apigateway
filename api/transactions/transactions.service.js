"use strict";

const {
  cancelTransactions_Spec,
  checkoutTransactions_Spec,
  confirmTransactions_Spec,
} = require("./transactions.spec");

const {
  checkoutTransactions_Module,
  cancelTransactions_Module,
  getTransactions_Module,
  confirmTransactions_Module,
} = require("./transactions.module");

/**
 * getChart_Service
 * @function addItemChart_Service services get chart
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
async function getAllTransactions_Service() {
  try {
    const res = await getTransactions_Module();
    return {...res};
  } catch (error) {
    return {error: true, message: error, data: {}};
  }
}

/**
 * checkoutTransactions_Service
 * @param {object} payload
 * @param {number} payload.invoiceNumber invoiceNumber with UUID
 * @param {number} payload.price total price transactions
 * @param {Array.<{courseId: string, quantity: number, price: number,totalPrice:number }>} payload.listItem Only allow courseId and quantity
 * @param {string} payload.status enum confirm|cancel|pending
 * @example * var payload ={ invoiceNumber:1234, price:1200, listItem : [{courseId:'abcdefg', quantity:2, price:600, totalPrice:1200}], status:'pending' }*
 * @returns {Promise<{ error:boolean, message:string, data:{invoiceNumber:number} }>}
 */
async function checkoutTransactions_Service(payload) {
  try {
    let res = await checkoutTransactions_Spec(payload);
    if (!res.error) {
      res = await checkoutTransactions_Module(payload);
    }
    return {...res};
  } catch (error) {
    return {error: true, message: error, data: {}};
  }
}

/**
 * confirmTransactions_Service
 * @function confirmTransactions_Service services edit quantity item
 * @param {object} payload
 * @param {string} payload.invoiceNumber
 * @example *var payload = { invoiceNumber:'123'}*
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
async function confirmTransactions_Service(payload) {
  try {
    let res = await confirmTransactions_Spec(payload);
    if (!res.error) {
      res = await confirmTransactions_Module(payload);
    }
    return {...res};
  } catch (error) {
    return {error: true, message: error, data: {}};
  }
}

/**
 * cancelTransactions_Service
 * @function cancelTransactions_Service services cancel transactions required Invoice Number
 * @param {object} payload
 * @param {string} payload.invoiceNumber
 * @example *var payload = { invoiceNumber:'123' }*
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
async function cancelTransactions_Service(payload) {
  try {
    let res = await cancelTransactions_Spec(payload);
    if (!res.error) {
      res = await cancelTransactions_Module(payload);
    }
    return {...res};
  } catch (error) {
    return {error: true, message: error, data: {}};
  }
}

module.exports = {
  getAllTransactions_Service,
  checkoutTransactions_Service,
  confirmTransactions_Service,
  cancelTransactions_Service,
};
