const DB = require("../../database");
const transactions = require("../../database/schema/transactions");

async function connectionFactory(dbName = "ecommerce", schemaName = transactions.model) {
  const conn = await DB(dbName);
  const model = conn.model(schemaName, transactions.schema, "transactions");
  return {conn, model};
}

/**
 * getTransactions_Module
 * @function getTransactions_Module module for get all transactions
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
exports.getTransactions_Module = async function (payload) {
  const {conn, model} = await connectionFactory();
  const data = await model.find().exec();

  let response = {error: false, message: "ok", data};

  await conn.close();
  return {...response};
};

/**
 * checkoutTransactions_Module
 * @function checkoutTransactions_Module module for add transactions, after checkout
 * @param {object} payload
 * @param {string} payload.invoiceNumber  payload from request
 * @param {number} payload.price
 * @param {object.Array.<{ courseId: string, quantity: number, price:number,totalPrice:number }>} payload.listItem
 * @example *var payload = { invoiceNumber:121233,price:12000, listItem :[{ courseId:'123', quantity:1, price:10000, totalPrice:10000 },{ courseId:'345', quantity:2, price:1000, totalPrice:2000 },...n]} }*
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
exports.checkoutTransactions_Module = async function (payload) {
  let response = {error: false, message: "ok", data: {}};
  const {conn, model} = await connectionFactory();
  const action = new model(payload);
  const save = await action.save();

  if (!save) {
    response.error = true;
    response.message = "store failed";
  }

  await conn.close();
  return {...response};
};

/**
 * cancelTransactions_Module
 * @function cancelTransactions_Module module for update status transactions to cancel
 * @param {object} payload  payload from request
 * @param {string} payload.invoiceNumber
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
exports.cancelTransactions_Module = async function (payload) {
  const {conn, model} = await connectionFactory();
  const action = await model.findOneAndUpdate(
    {invoiceNumber: payload.invoiceNumber},
    {$set: {status: "cancel"}}
  );

  let response = {error: false, message: "ok", data: {}};
  if (!action) {
    response.error = true;
    response.message = "remove failed";
  }

  await conn.close();
  return {...response};
};

/**
 * confirmTransactions_Module
 * @function confirmTransactions_Module module for update status transactions to confirm
 * @param {object} payload  payload from request
 * @param {string} payload.invoiceNumber
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
exports.confirmTransactions_Module = async function (payload) {
  const {conn, model} = await connectionFactory();
  const action = await model.findOneAndUpdate(
    {invoiceNumber: payload.invoiceNumber},
    {$set: {status: "confirm"}}
  );

  let response = {error: false, message: "ok", data: {}};
  if (!action) {
    response.error = true;
    response.message = "remove failed";
  }

  await conn.close();
  return {...response};
};
