const Joi = require("joi");

const item = {
  courseId: Joi.string().invalid("", null).required(),
  quantity: Joi.number().positive().required(),
  price: Joi.number().min(0).default(0).required(),
  totalPrice: Joi.number().min(0).default(0).required(),
};

const listItem = Joi.array().items(Joi.object(item));

const schemaCheckoutTransactions = Joi.object({
  invoiceNumber: Joi.string().invalid("", null).required(),
  price: Joi.number().positive().required(),
  listItem,
  status: Joi.string().default("pending").valid("confirm", "pending", "cancel").required(),
});

const schemaConfirmTransactions = Joi.object({
  invoiceNumber: Joi.string().invalid("", null).required(),
  price: Joi.number().positive().required(),
});

const schemaCancelTransactions = Joi.object({
  invoiceNumber: Joi.string().invalid("", null).required(),
});

/**
 * checkout transaction
 * @param {object} payload
 * @param {number} payload.invoiceNumber invoiceNumber with UUID
 * @param {number} payload.price total price transactions
 * @param {Array.<{courseId: string, quantity: number, price: number,totalPrice:number }>} payload.listItem Only allow courseId and quantity
 * @param {string} payload.status enum confirm|cancel|pending
 * @example * var payload ={ invoiceNumber:1234, price:1200, listItem : [{courseId:'abcdefg', quantity:2, price:600, totalPrice:1200}], status:'pending' }*
 * @returns {Promise<{ error:boolean, message:string, data:{invoiceNumber:number} }>}
 */
exports.checkoutTransactions_Spec = async function (payload) {
  const {value, error} = schemaCheckoutTransactions.validate(payload);
  return {message: error ? error : "ok", error: error, data: value};
};

/**
 * confirm transaction
 * @param {object} payload
 * @param {number} payload.invoiceNumber invoiceNumber with UUID
 * @param {number} payload.price total price transactions
 * @example * var payload ={ invoiceNumber:1234, price:1200 }*
 * @returns {Promise<{ error:boolean, message:string, data:{invoiceNumber:number} }>}
 */
exports.confirmTransactions_Spec = async function (payload) {
  const {value, error} = schemaConfirmTransactions.validate(payload);
  return {message: error ? error : "ok", error: error, data: value};
};

/**
 * cancel transaction
 * @param {object} payload
 * @param {number} payload.invoiceNumber invoiceNumber with UUID
 * @example * var payload ={ invoiceNumber:1234 }*
 * @returns {Promise<{ error:boolean, message:string, data:{invoiceNumber:number} }>}
 */
exports.cancelTransactions_Spec = async function (payload) {
  const {value, error} = schemaCancelTransactions.validate(payload);
  return {message: error ? error : "ok", error: error, data: value};
};
