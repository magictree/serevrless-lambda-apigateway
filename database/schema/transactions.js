const mongoose = require("mongoose");

const model = "transactions";

const schema = new mongoose.Schema(
  {
    invoiceNumber: {type: String, uniqe: true, required: [true, "invoice number is required!"]},
    listItem: [
      {
        courseId: {
          type: mongoose.Schema.ObjectId,
          required: [true, "courseId is required!"],
          uniqe: true,
        },
        quantity: {type: Number, min: 1, required: [true, "quantity is required!"]},
        price: {type: Number, min: 0, required: [true, "price is required!"]},
        totalPrice: {type: Number, min: 0, required: [true, "total price is required!"]},
      },
    ],
    price: {type: Number, min: 0, required: [true, "price is required!"]},
    status: {type: String, default: "pending"}, // cancel, confirm, pending
  },
  {
    timestamps: true,
  }
);

module.exports = {schema, model};
