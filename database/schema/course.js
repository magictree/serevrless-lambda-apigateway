const mongoose = require('mongoose');

const model = 'course';

const schema = new mongoose.Schema(
  {
    title: {type: String, required: [true, 'Title course is required!'], uniqe: true},
    description: {type: String},
    price: {type: Number, min: 0, default: 0},
    is_free: {type: Boolean, default: true},
    author: {type: String, default: 'agustriadji'},
    status: {type: String, default: 'active'},
  },
  {
    timestamps: true,
  }
);

module.exports = {schema, model};
