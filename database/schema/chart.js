const mongoose = require('mongoose');

const model = 'chart';

const schema = new mongoose.Schema(
  {
    listItem: [
      {
        courseId: {
          type: mongoose.Schema.ObjectId,
          required: [true, 'courseId is required!'],
          uniqe: true,
        },
        quantity: {type: Number, min: 1, required: [true, 'quantity is required!']},
        price: {type: Number, min: 0, default: 0},
      },
    ],
  },
  {
    timestamps: true,
  }
);

module.exports = {schema, model};
