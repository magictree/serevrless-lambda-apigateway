require("dotenv").config();
const mongoose = require("mongoose");

/**
 *
 * @desciption cannot read file .env, mesti di deploy dulu
 */
async function DB(dbName) {
  let Connection = null;

  if (dbName === "lms") {
    dbName = process.env.MONGODB_LMS_HOST;
  } else if (dbName === "ecommerce") {
    dbName = process.env.MONGODB_ECOMMERCE_HOST;
  } else {
    return {error: true, message: "Service not available"};
  }

  if (Connection) Connection.close();

  Connection = mongoose.createConnection(dbName);
  Connection.set("debug", true);

  return Connection;
}

module.exports = DB;
